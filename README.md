**DB Schema


sqlite3 usermanagement.db

CREATE TABLE role(
 id int PRIMARY KEY,
 role text
);

CREATE TABLE user (
 id int PRIMARY KEY,
 name Varchar,
 email Varchar,
 password Varchar,
 phone int,
 role_id int,
 FOREIGN KEY(role_id) REFERENCES role(id)
);

CREATE TABLE user_role_mapping(
 id int PRIMARY KEY, 
 user_id int, 
 role_id int, 
 FOREIGN KEY(user_id) REFERENCES user(id), 
 FOREIGN KEY(role_id) REFERENCES role(id)
);





