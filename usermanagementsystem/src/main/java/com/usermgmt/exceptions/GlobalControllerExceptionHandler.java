package com.usermgmt.exceptions;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.StaleStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(NotFoundException ex) {
        ApiError apiError = new ApiError(NOT_FOUND);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }



    @ExceptionHandler(OperationNotAllowedException.class)
    protected ResponseEntity<Object> handleOperationNotAllowedException(OperationNotAllowedException ex) {
        ApiError apiError = new ApiError(NOT_ACCEPTABLE);
        apiError.setMessage(ex.getMessage());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(StaleStateException.class)
    protected ResponseEntity<Object> handleStaleStateException(StaleStateException ex) {
        ApiError apiError = new ApiError(CONFLICT);
        apiError.setMessage("Other user is trying to update this account. Please try after some time.");
        if(ex.getStackTrace()!=null && ex.getStackTrace().length>0){
            ex.printStackTrace();
            String rootCause = ExceptionUtils.getRootCauseMessage(ex.getCause());
            apiError.setDebugMessage(" at " +ex.getStackTrace()[0].getLineNumber()
                    +" in method "+ex.getStackTrace()[0].getMethodName()
                    +" in class "+ex.getStackTrace()[0].getClassName()
                    +"with rootCause "+rootCause
            );
        }
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(OptimisticLockException.class)
    protected ResponseEntity<Object> handleOptimisticLockException(OptimisticLockException ex) {
        ApiError apiError = new ApiError(LOCKED);
        apiError.setMessage("Other user is trying to update this account. Please try after some time.");
        if(ex.getStackTrace()!=null && ex.getStackTrace().length>0){
            ex.printStackTrace();
            String rootCause = ExceptionUtils.getRootCauseMessage(ex.getCause());
            apiError.setDebugMessage(" at " +ex.getStackTrace()[0].getLineNumber()
                    +" in method "+ex.getStackTrace()[0].getMethodName()
                    +" in class "+ex.getStackTrace()[0].getClassName()
                    +"with rootCause "+rootCause
            );
        }
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(RollbackException.class)
    protected ResponseEntity<Object> handlRollbackException(RollbackException ex) {
        ApiError apiError = new ApiError(NOT_MODIFIED);
        apiError.setMessage("Other user is trying to update this account. Please try after some time.");
        if(ex.getStackTrace()!=null && ex.getStackTrace().length>0){
            ex.printStackTrace();
            String rootCause = ExceptionUtils.getRootCauseMessage(ex.getCause());
            apiError.setDebugMessage(" at " +ex.getStackTrace()[0].getLineNumber()
                    +" in method "+ex.getStackTrace()[0].getMethodName()
                    +" in class "+ex.getStackTrace()[0].getClassName()
                    +"with rootCause "+rootCause
            );
        }
        return buildResponseEntity(apiError);
    }





    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }


    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> genericExceptionHandler(Exception ex){
        ApiError apiError = new ApiError(INTERNAL_SERVER_ERROR);
        if(ex.getStackTrace()!=null && ex.getStackTrace().length>0){
            ex.printStackTrace();
            String rootCause = ExceptionUtils.getRootCauseMessage(ex.getCause());
            String msg = "";
            if(ex.getMessage()!=null){
                msg = ex.getMessage();
            }
            apiError.setDebugMessage(msg+" at " +ex.getStackTrace()[0].getLineNumber()
                    +" in method "+ex.getStackTrace()[0].getMethodName()
                    +" in class "+ex.getStackTrace()[0].getClassName()
                    +"with rootCause "+rootCause
                    );
        }
        apiError.setMessage("There was an error in performing this operation.");
        return buildResponseEntity(apiError);
    }



}
