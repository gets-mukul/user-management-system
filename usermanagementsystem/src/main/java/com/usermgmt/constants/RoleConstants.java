/**
 * 
 */
package com.usermgmt.constants;

/**
 * @author MukulP
 *
 */
public class RoleConstants {

	public static final String EMPLOYEE = "Employee";
	public static final String ADMIN = "Admin";
	public static final String MANAGER = "Manager";

}
