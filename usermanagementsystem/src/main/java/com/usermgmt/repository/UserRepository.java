/**
 * 
 */
package com.usermgmt.repository;

import com.usermgmt.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author MukulP
 *
 */

public interface UserRepository extends JpaRepository<User, Integer> {

	@Override
	@Query("select usr from User usr left join fetch usr.role")
	List<User> findAll();

}
