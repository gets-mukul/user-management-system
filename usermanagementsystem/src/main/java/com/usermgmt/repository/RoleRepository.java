/**
 *
 */
package com.usermgmt.repository;

import com.usermgmt.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author MukulP
 *
 */
public interface RoleRepository extends JpaRepository<Role, Integer>{
    List<Role> findByRoleType(String roleType);
}
