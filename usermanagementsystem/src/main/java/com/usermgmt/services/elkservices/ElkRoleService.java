package com.usermgmt.services.elkservices;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.usermgmt.configuration.ApplicationContextProvider;
import com.usermgmt.entity.Role;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ElkRoleService {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static Client client = ApplicationContextProvider.getBean(Client.class);

    public void addRole(Role entity) {


        Map<String, String> roleJson = new HashMap<>();
        roleJson.put("id", entity.getId().toString());
        roleJson.put("roleType", entity.getRoleType());

        IndexResponse response = client.prepareIndex("role", "role", entity.getId().toString())
                .setSource(roleJson)
                .get();


    }

    public void deleteRole(Integer id) {
        DeleteResponse response = client.prepareDelete("role", "role", id.toString()).get();
    }

    public List<Role> getAllRoles() throws IOException {
        List<Role> roles = new ArrayList<>();

        SearchResponse response = client.prepareSearch("role").get();
        SearchHit[] jsonData = response.getHits().getHits();

        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        for (int i = 0; i < jsonData.length; i++) {
            Role role = mapper.readValue(jsonData[i].getSourceAsString(), Role.class);
            roles.add(role);
        }
        return roles;
    }
}
