package com.usermgmt.services.elkservices;

import com.usermgmt.configuration.ApplicationContextProvider;
import com.usermgmt.entity.User;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class ElkUserService {

    private static Client client = ApplicationContextProvider.getBean(Client.class);

    public void addUser(User entity) {



        Map<String, String> userJson = new HashMap<String, String>();
        userJson.put("id", entity.getId().toString());
        userJson.put("name", entity.getName());
        userJson.put("email", entity.getEmail());
        userJson.put("phone", entity.getPhone().toString());
        userJson.put("password", entity.getPassword());
        userJson.put("role", entity.getRole().getRoleType());

        IndexResponse response = client.prepareIndex("user", "user", entity.getId().toString())
                .setSource(userJson)
                .get();
    }

    public void deleteUser(Integer id) {
        DeleteResponse response = client.prepareDelete("user", "user", id.toString()).get();
    }

}
