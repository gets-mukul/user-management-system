/**
 * 
 */
package com.usermgmt.services;

import com.usermgmt.controller.role.pojo.CreateRoleRequest;
import com.usermgmt.entity.Role;
import com.usermgmt.exceptions.OperationNotAllowedException;

import java.util.List;
import java.util.Optional;

/**
 * @author MukulP
 *
 */

public interface RoleService {

	public List<Role> getAllRoles() throws OperationNotAllowedException;

	public Role addRole(CreateRoleRequest request) throws OperationNotAllowedException;

	public Optional<Role> deleteRole(Integer id) throws OperationNotAllowedException;

	public Role getRoleByType(String roleType) throws OperationNotAllowedException;

}
