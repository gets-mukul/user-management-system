package com.usermgmt.services.kafkaservices;

import com.usermgmt.configuration.KafkaProducerConfig;
import com.usermgmt.entity.User;
import com.usermgmt.services.elkservices.ElkUserService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class KafkaDemo {

    private ElkUserService elkUserService;
    private KafkaTemplate<String, User> kafkaTemplate;

    public KafkaDemo() {
        this.elkUserService = new ElkUserService();
        KafkaProducerConfig config = new KafkaProducerConfig();
        this.kafkaTemplate = config.kafkaTemplate();
    }

    public void userProducer(User user) {
        ListenableFuture<SendResult<String, User>> future = kafkaTemplate.send("user", user);

        future.addCallback(new ListenableFutureCallback<SendResult<String, User>>() {

            @Override
            public void onSuccess(SendResult<String, User> result) {
                System.out.println("Produced message=[" + user.getName() +
                        "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }

            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to produce message=["
                        + user + "] due to : " + ex.getMessage());
            }
        });
    }

    @KafkaListener(topics = "user", groupId = "user")
    public void listen(ConsumerRecord<String, User> user) throws InterruptedException {
//        Thread.sleep(1000);
        elkUserService.addUser(user.value());
        System.out.println("=====================User data consumed: " + user.value().getName());
    }


}
