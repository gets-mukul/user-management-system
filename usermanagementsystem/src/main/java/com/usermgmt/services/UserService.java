/**
 * 
 */
package com.usermgmt.services;

import com.usermgmt.controller.user.pojo.CreateUserRequest;
import com.usermgmt.controller.user.pojo.UserLoginRequest;
import com.usermgmt.entity.User;
import com.usermgmt.exceptions.OperationNotAllowedException;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author MukulP
 *
 */

public interface UserService {

	public List<User> getAllUsers();

	public User removeUser(Integer id) throws OperationNotAllowedException;

	public User userRegisteration(CreateUserRequest request) throws OperationNotAllowedException;

	public User userLogin(UserLoginRequest request) throws OperationNotAllowedException;

	public User findByEmail(String email);

	public User updateUserRole(Integer employeeId) throws OperationNotAllowedException;

}
