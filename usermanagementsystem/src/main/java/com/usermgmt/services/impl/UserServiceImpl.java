/**
 *
 */
package com.usermgmt.services.impl;

import com.usermgmt.constants.RoleConstants;
import com.usermgmt.controller.user.pojo.CreateUserRequest;
import com.usermgmt.controller.user.pojo.UserLoginRequest;
import com.usermgmt.entity.Role;
import com.usermgmt.entity.User;
import com.usermgmt.exceptions.OperationNotAllowedException;
import com.usermgmt.repository.UserRepository;
import com.usermgmt.services.UserService;
import com.usermgmt.services.elkservices.ElkUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author MukulP
 *
 */
@Service
public class UserServiceImpl implements UserService, ApplicationEventPublisherAware {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public RoleServiceImpl roleServiceImpl;

    @Autowired
    public ApplicationEventPublisher publisher;

    @Autowired
    ElkUserService elkUserService;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User removeUser(Integer id) throws OperationNotAllowedException {
        User user = findById(id);

        if (user == null) {
            throw new OperationNotAllowedException("User Does not Exist with this ID");
        }
        userRepository.deleteById(id);
        elkUserService.deleteUser(id); // Delete User from ElasticSearch

        return user;


    }

    public User userRegisteration(CreateUserRequest request) throws OperationNotAllowedException {

        User userObj = findByEmail(request.getEmail());
        if (userObj != null) {
            throw new OperationNotAllowedException("User Already Exist with this email");
        }

        User user = User.builder().name(request.getName()).email(request.getEmail()).password(request.getPassword()).phone(request.getPhone()).build();
        Role role = roleServiceImpl.getRoleByType(RoleConstants.EMPLOYEE);
        user.setRole(role);
        user = userRepository.save(user);

        return user;
    }

    public User userLogin(UserLoginRequest request) throws OperationNotAllowedException {
        User userObj = findByEmail(request.getEmail());

        if (userObj == null) {
            throw new OperationNotAllowedException("User does not exist with this email");
        } else if (!userObj.getPassword().equals(request.getPassword())) {
            throw new OperationNotAllowedException("User does not exist with this password");
        }

        return userObj;
    }

    public User findByEmail(String email) {
        List<User> listOfUser = getAllUsers();
        for (User entity : listOfUser) {
            if (entity.getEmail().equals(email)) {
                return entity;
            }
        }
        return null;
    }

    public User findById(Integer id) {
        List<User> users = userRepository.findAll();
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public ResponseEntity<String> forgotPassword(String email, String newPass) {
        User userObj = findByEmail(email);
        if (userObj != null) {
            userObj.setPassword(newPass);
            userRepository.save(userObj);
            return new ResponseEntity<String>("Password changed successfully", HttpStatus.OK);
        }
        return new ResponseEntity<String>("Please enter correct email", HttpStatus.NOT_ACCEPTABLE);
    }

    public User updateUserRole(Integer employeeId) throws OperationNotAllowedException {

        User employee = findById(employeeId);

        if (employee == null) {
            throw new OperationNotAllowedException("User doesn't exist with this ID");
        }

        Role newRole = roleServiceImpl.getRoleByType(RoleConstants.ADMIN);
        employee.setRole(newRole);
        userRepository.save(employee);
        return employee;
    }


    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {

        this.publisher = publisher;

    }

}
