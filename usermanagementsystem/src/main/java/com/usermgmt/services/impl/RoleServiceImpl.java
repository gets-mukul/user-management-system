/**
 *
 */
package com.usermgmt.services.impl;

import com.usermgmt.controller.role.pojo.CreateRoleRequest;
import com.usermgmt.entity.Role;
import com.usermgmt.exceptions.OperationNotAllowedException;
import com.usermgmt.repository.RoleRepository;
import com.usermgmt.services.RoleService;
import com.usermgmt.services.elkservices.ElkRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author MukulP
 *
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    public RoleRepository roleRepository;

    @Autowired
	ElkRoleService elkRoleService;

    @Transactional
    public List<Role> getAllRoles() throws OperationNotAllowedException {
        List<Role> roles = roleRepository.findAll();

        if (roles.isEmpty()) {
            throw new OperationNotAllowedException("No Roles Exist");
        }

        return roles;
    }


    public Role addRole(CreateRoleRequest request) throws OperationNotAllowedException {

        List<Role> existingRoles = roleRepository.findByRoleType(request.getRoleName());
        if (!existingRoles.isEmpty()) {
            throw new OperationNotAllowedException("Role with requested name already exist.");
        }

        Role role = Role.builder().roleType(request.getRoleName()).build();
        role = roleRepository.save(role);

        return role;
    }

    public Optional<Role> deleteRole(Integer id) throws OperationNotAllowedException {
        Optional<Role> role = roleRepository.findById(id);

        if (role.isEmpty()) {
            throw new OperationNotAllowedException("Role does not Exist for this Id");
        }

        roleRepository.deleteById(id);

		elkRoleService.deleteRole(id);  //delete from ElasticSearch

        return role;
    }

    public Role getRoleByType(String roleType) throws OperationNotAllowedException {

        List<Role> listOfRoles = getAllRoles();

        for (Role entity : listOfRoles) {
            if (entity.getRoleType().equalsIgnoreCase(roleType)) {
                return entity;
            }
        }
        return null;

    }

}
