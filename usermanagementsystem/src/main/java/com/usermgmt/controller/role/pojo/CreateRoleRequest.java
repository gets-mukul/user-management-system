package com.usermgmt.controller.role.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateRoleRequest {

	@JsonProperty(value="role_name", required=true) private String roleName;
	@JsonProperty(value="created_by_id", required=true) private Integer createdById;

}
