/**
 * 
 */
package com.usermgmt.controller.role;

import com.usermgmt.controller.BaseResponse;
import com.usermgmt.controller.role.pojo.CreateRoleRequest;
import com.usermgmt.entity.Role;
import com.usermgmt.exceptions.OperationNotAllowedException;
import com.usermgmt.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author MukulP
 *
 */
@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	public RoleService roleService;

	@PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse addRole(@Valid @RequestBody CreateRoleRequest request) throws OperationNotAllowedException {
		Role role = roleService.addRole(request);
		return new BaseResponse(HttpStatus.CREATED.value(), "Role created successfully", role);
	}

	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse getAllRoles() throws OperationNotAllowedException {
		List<Role> roles = roleService.getAllRoles();
		return new BaseResponse(HttpStatus.OK.value(), "List of Roles", roles);
	}

	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse deleteRole(@PathVariable("id") Integer id) throws OperationNotAllowedException {
		Optional<Role> deletedRole = roleService.deleteRole(id);
		return new BaseResponse(HttpStatus.MOVED_PERMANENTLY.value(), "Role Deleted Successfully", deletedRole);
	}
}
