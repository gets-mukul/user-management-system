/**
 *
 */
package com.usermgmt.controller;

import com.usermgmt.constants.RoleConstants;
import com.usermgmt.entity.Role;
import com.usermgmt.entity.User;
import com.usermgmt.exceptions.OperationNotAllowedException;
import com.usermgmt.services.elkservices.ElkRoleService;
import com.usermgmt.services.impl.RoleServiceImpl;
import com.usermgmt.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.util.List;

/**
 * @author MukulP
 *
 */
@Controller
public class ViewController {

    @Autowired
    public RoleServiceImpl roleServiceImpl;

    @Autowired
    public UserServiceImpl userServiceImpl;

    @Autowired
    ElkRoleService elkRoleService;

    @GetMapping(value = "/")
    public String hello(@ModelAttribute("model") ModelMap model) throws OperationNotAllowedException, IOException {

        List<Role> roles = elkRoleService.getAllRoles();
        model.addAttribute("roles", roles);
        return "index";
    }

    @GetMapping(value = "/login/{email}")
    public String login(ModelMap model, @PathVariable("email") String email) {

        email += ".com";
        User user = userServiceImpl.findByEmail(email);

        if (user.getRole().getRoleType().equalsIgnoreCase(RoleConstants.ADMIN)) {
            List<User> users = userServiceImpl.getAllUsers();
            model.addAttribute("users", users);

            return "adminDashboard";
        } else {
            model.put("user", user);

            return "empDashboard";
        }

    }

    @GetMapping(value = "/login/admin")
    public String adminLogin(@ModelAttribute("model") ModelMap model) throws OperationNotAllowedException {
        List<User> users = userServiceImpl.getAllUsers();
        List<com.usermgmt.entity.Role> roles = roleServiceImpl.getAllRoles();
        model.addAttribute("roles", roles);
        model.addAttribute("users", users);

        return "adminDashboard";
    }

}
