package com.usermgmt.controller.user.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateUserRequest {

    @JsonProperty(value = "name", required = true)
    private String name;
    @JsonProperty(value = "email", required = true)
    private String email;
    @JsonProperty(value = "password", required = true)
    private String password;
    @JsonProperty(value = "phone", required = true)
    private Long phone;

}
