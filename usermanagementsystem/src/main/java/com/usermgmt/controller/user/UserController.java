package com.usermgmt.controller.user;

import com.usermgmt.controller.BaseResponse;
import com.usermgmt.controller.ViewController;
import com.usermgmt.controller.user.pojo.CreateUserRequest;
import com.usermgmt.controller.user.pojo.UserLoginRequest;
import com.usermgmt.entity.User;
import com.usermgmt.exceptions.OperationNotAllowedException;
import com.usermgmt.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author MukulP
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    public UserServiceImpl userServiceImpl;

    @Autowired
    public ViewController viewController;

    @GetMapping(value = "/list")
    public List<User> getAllUsers() {
        return userServiceImpl.getAllUsers();

    }

    @DeleteMapping(value = "/delete/{id}")
    public BaseResponse removeUser(@PathVariable("id") Integer id) throws OperationNotAllowedException {

        User user = userServiceImpl.removeUser(id);
        return new BaseResponse(HttpStatus.MOVED_PERMANENTLY.value(), "User Deleted Successfully", user);

    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse registerUser(@Valid @RequestBody CreateUserRequest request) throws OperationNotAllowedException {
        User user = userServiceImpl.userRegisteration(request);
        return new BaseResponse(HttpStatus.CREATED.value(), "User created Successfully", user);
    }

    @PostMapping(value = "/login")
    public BaseResponse userLogin(@Valid @RequestBody UserLoginRequest request) throws OperationNotAllowedException {
        User user = userServiceImpl.userLogin(request);
        return new BaseResponse(HttpStatus.ACCEPTED.value(), "Login Successful", user);

    }

    @PutMapping(value = "/update/role")
    public BaseResponse updateRole(@RequestParam Integer employeeId) throws OperationNotAllowedException {
        User user = userServiceImpl.updateUserRole(employeeId);
        return new BaseResponse(HttpStatus.OK.value(), "User Role Updated", user);
    }
}