/**
 * 
 */
package com.usermgmt.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author MukulP
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse{
	
	
	public BaseResponse(Integer statusCode, String message, Object data) {
		this.statusCode = statusCode;
		this.message = message;
		this.data = data;
	}

	@JsonProperty("status_code") private Integer statusCode;
	@JsonProperty("message") private String message;
	@JsonProperty("data") private Object data;
	
}
