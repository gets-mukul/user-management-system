/**
 * 
 */
package com.usermgmt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;

/**
 * @author MukulP
 *
 */
@Entity
@Table(name = "role", schema = "user_management_db")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "role", type = "role")

public class Role {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;
    @Column(name = "role_type")
	private String roleType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "created_by", referencedColumnName = "id", insertable = true, updatable = true)
    private User createdBy;


}
