/**
 * 
 */
package com.usermgmt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;

/**
 * @author MukulP
 *
 */
@Entity
@Table(name = "user", schema = "user_management_db")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "user", type = "user")

public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;
    @Column(name = "name")
	private String name;
    @Column(name = "email")
	private String email;
    @Column(name = "phone")
	private Long phone;
    @Column(name = "password")
	private String password;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", referencedColumnName = "id", insertable = true, updatable = true)
	private Role role;

}
