package com.usermgmt.interceptors;

import com.usermgmt.configuration.ApplicationContextProvider;
import com.usermgmt.entity.Role;
import com.usermgmt.entity.User;
import com.usermgmt.services.elkservices.ElkRoleService;
import com.usermgmt.services.kafkaservices.KafkaDemo;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Iterator;

public class CustomIntercepter extends EmptyInterceptor {


    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

        if (entity instanceof Role) {
            ApplicationContextProvider.getBean(ElkRoleService.class).addRole((Role) entity);
        }
//        else if (entity instanceof User) {
//            ApplicationContextProvider.getBean(ElkUserService.class).addUser((User) entity);
//        }

        return super.onSave(entity, id, state, propertyNames, types);

    }

    @Override
    public void postFlush(Iterator entities) {
        while (entities.hasNext()) {
            Object entity = entities.next();

            if (entity instanceof User) {
                System.out.println(((User) entity).getId());
                KafkaDemo demo = new KafkaDemo();
                demo.userProducer((User) entity);
            }
        }
        super.postFlush(entities);
    }
}
