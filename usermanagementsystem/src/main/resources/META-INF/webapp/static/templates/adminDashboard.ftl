<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="../webapp/static/js/adminDashboard.js"></script>



</head>

<body>
<h2>Welcome to Admin Dashboard</h2>
<div class="container">
  <h2>User Details</h2>        
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Role</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    <#list model["users"] as user>
      <tr>
        <th scope="col">${user.id!}</th>
        <td scope="col">${user.name!}</td>
        <td scope="col">${user.email!}</td>
        <td scope="col">${user.phone!}</td>
        <td scope="col">${user.role.getRoleType()!}</td>
        <td scope="col"><button type="button" id="${user.id!}" class="btn btn-danger delete_user_btn">Delete</button></td>
        <td scope="col">
        <#if user.role.getRoleType() != "admin">
  			<button type="button" id="${user.id!}" class="btn btn-success make_admin_btn">Make Admin</button>
		</#if>
        </td>
      </tr>
  	</#list>
  </tbody>
  </table>
</div>
<button type="button" class="btn btn-danger" id="logout_btn">Logout</button>

</body>
</html>