<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script type="text/javascript" src="../webapp/static/js/index.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


</head>

<body>

<h2>Welcome to User Management System</h2>
<br>
<div class="row">
	<div class="col-sm-4"><button type="button" class="btn btn-success btn-lg" id="login_btn">Login</button></div>
	<div class="col-sm-4"><button type="button" class="btn btn-success btn-lg" id="signup_btn">Sign Up</button></div>
	<div class="col-sm-4"><button type="button" class="btn btn-success btn-lg" id="add_role_btn">Add New Role</button></div>

</div>

<!-- Role modal -->

<div class="modal fade bd-example-modal-sm" id="role_list_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
 <table class="table">
  <thead>
	<tr>
      <th scope="col">New Role</th>
      <th scope="col"><input type="text" class="form-control" id="input_role"></th>
      <th scope="col"><button type="button" class="btn btn-success" id="new_role_btn">Create</button></th>
    </tr>

    <tr>
      <th scope="col">#</th>
      <th scope="col">Roles</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <#list model["roles"] as role>
      <tr>
        <th scope="col">${role.id}</th>
        <td scope="col">${role.roleType}</td>
        <td scope="col"><button type="button" id="${role.id}" class="btn btn-danger del_role_btn">Delete</button></td>
      </tr>
  </#list>
  </tbody>
</table>
    </div>
  </div>
</div>

<!-- Sign Up modal -->

<div class="modal fade bd-example-modal-sm" id="signup_modal" tabindex="-1" role="dialog" aria-labelledby="signUpMpdal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
 		<form>
 			<h2>User Registration</h2>
 			<div class="form-group">
 		      <label for="name">Name:</label>
 		      <input type="text" class="form-control" id="signup_name" placeholder="Enter name" name="name">
 		    </div>
 		    <div class="form-group">
 		      <label for="email">Email:</label>
 		      <input type="email" class="form-control" id="signup_email" placeholder="Enter email" name="email">
 		    </div>
 		    <div class="form-group">
 		      <label for="pwd">Password:</label>
 		      <input type="password" class="form-control" id="signup_pwd" placeholder="Enter password" name="pswd">
 		    </div>
 		    <div class="form-group">
 		      <label for="phone">Phone:</label>
 		      <input type="number" class="form-control" id="signup_phone" placeholder="Enter Phone" name="phone">
 		    </div>
 		    <div class="form-group form-check">
 		      <label class="form-check-label">
 		        <input class="form-check-input" type="checkbox" name="remember"> Remember me
 		      </label>
 		    </div>
          <button type="button" class="btn btn-success" id="signup_modal_btn">Sign Up</button>

 		  </form>
    </div>
  </div>
</div>


<!-- Login modal -->

<div class="modal fade bd-example-modal-sm" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
 		<form>
 		<h2>User Login</h2>
 		    <div class="form-group">
 		      <label for="email">Email:</label>
 		      <input type="email" class="form-control" id="login_email" placeholder="Enter email" name="email">
 		    </div>
 		    <div class="form-group">
 		      <label for="pwd">Password:</label>
 		      <input type="password" class="form-control" id="login_pwd" placeholder="Enter password" name="pswd">
 		    </div>
 		    <div class="form-group form-check">
 		      <label class="form-check-label">
 		        <input class="form-check-input" type="checkbox" name="remember"> Remember me
 		      </label>
 		    </div>
          <button type="button" class="btn btn-success" id="login_modal_btn">Login</button>
 		  </form>
    </div>
  </div>
</div>





</body>
</html>