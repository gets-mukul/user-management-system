$(document).ready(function(){
	
	$("#role_list_modal").modal('show');
	
	$("#add_role_btn").on("click", function() {
  		$("#role_list_modal").modal('show');
	});


	$("#signup_btn").on("click", function() {
  		$("#signup_modal").modal('show');
	});

	$("#login_btn").on("click", function() {
  		$("#login_modal").modal('show');
	});
	
	
        
	$("#new_role_btn").click(function(){

		var roleType = $("#input_role").val();

		var jsonData = JSON.stringify({"role_name": roleType, "created_by_id": 0});

		$.ajax({

			url:"/role/add",
			type:"POST",
			data: jsonData,
			contentType: 'application/json',
			success : function(response) {
				swal("New Role Created!", "", "success").then((value) => {
 					 location.reload(true);
				});

		    },
		    error : function(e)
		    {
		        alert(e.message);
		    }


		});
	});
	
	
	$(".del_role_btn").click(function(){
	
	var roleId = $(this).attr('id');
	
		$.ajax({
			
				url:"/role/delete/"+roleId,
				type:"DELETE",
				success : function() {
					swal("Role Deleted Successfully", "", "error").then((value) => {
	 					 location.reload(true);
					});
			        
			    },
			    error : function(e)
			    {
			        alert(e.message);
			    }
			    
			
		});
		
		
	});
	
	
	
	$("#signup_modal_btn").click(function(){
		var name = $("#signup_name").val();
		var email = $("#signup_email").val();
		var password = $("#signup_pwd").val();
		var phone = $("#signup_phone").val();
		
		var jsonData = JSON.stringify({"name": name, "email": email, "password": password, "phone": phone});
		
		
		$.ajax({
			
				url:"/user/register",
				type:"POST",
				data: jsonData,
				contentType: 'application/json',
				success : function() {
					swal("New User Created!", "", "success").then((value) => {
 					 location.reload(true);
				});
			    },
			    error : function(e)
			    {
			        alert(e.message);
			    }
			    
			
		});
	
		
	
	});
	
	
	
	$("#login_modal_btn").click(function(){
		var email = $("#login_email").val();
		var password = $("#login_pwd").val();
		
		var jsonData = JSON.stringify({"email": email, "password": password});
		
		
		$.ajax({
			
				url:"/user/login",
				type:"POST",
				data: jsonData,
				contentType: 'application/json',
				success : function(response) {

                var role = response.data.role.roleType;
                alert(role);
					if(role.toUpperCase() == "ADMIN"){
						location.href = "http://localhost:8080/login/admin";
					}
					else{
						location.href = "http://localhost:8080/login/"+email;
					}

			    },
			    error : function(e)
			    {
			        alert(e.message);
			    }
			    
			
		});
		
	
		
	
	});
	
	
	
	
	
	
        
});