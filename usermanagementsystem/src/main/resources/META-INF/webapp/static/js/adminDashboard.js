$(document).ready(function() {
	
	$("#logout_btn").on("click", function() {
		location.href = "http://localhost:8080";
	});
	
	$(".delete_user_btn").on("click", function() {
		var id = $(this).attr('id');
		var jsonData = {"id": id};
		$.ajax({
			
			url:"/user/delete/"+id,
			type:"DELETE",
			data: jsonData,
			contentType: 'application/json',
			success : function() {
				swal("User Deleted Successfully!", "", "success").then((value) => {
 					 location.reload(true);
				});
		        
		    },
		    error : function(e)
		    {
		        alert(e.message);
		    }
		    
		
		});
	});
	
	$(".make_admin_btn").on("click", function() {
		var id = $(this).attr('id');
		var jsonData = {"id": id};
	
		
		$.ajax({
			
			url:"/user/update/role?employeeId="+id,
			type:"PUT",
			data: jsonData,
			success : function() {
				swal("User Updated Successfully!", "", "success").then((value) => {
 					 location.reload(true);
				});
		        
		    },
		    error : function(e)
		    {
		        alert(e.message);
		    }
		    
		
		});
		$(this).attr("disabled", true);
		
		
	});
	
	
	
	 
});